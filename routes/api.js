const log = require('log4js').getLogger('api')
const { tokenCreate, tokenValidate } = require('../models/utility')
const db = require('../models/db')

const TOKEN_LONG_INTERVAL = (process.env.TOKEN_LONG_INTERVAL || (60 * 60 * 24 * 7)) * 1000
const TOKEN_SHORT_INTERVAL = (process.env.TOKEN_SHORT_INTERVAL || (60 * 60 * 1)) * 1000

module.exports = {

  'auth.generic': ({ guid }, callback) => {
    log.debug(`auth.generic ${guid}`)
    db.authGeneric(guid)
    .then(profile_id => {
      let token = tokenCreate({
        profile_id
      }, TOKEN_LONG_INTERVAL)
      callback(null, {
        token
      })
    })
    .catch(error => {
      callback(error)
    })
  },

  'space.list': ({ token }, callback) => {
    log.debug(`space.list`)
    tokenValidate(token)
    .then(({ profile_id }) => {
      db.spaceList(profile_id)
      .then(spaces => {
        callback(null, spaces)
      })
    })
    .catch(error => {
      callback(error)
    })
  },

  'space.new': ({ token }, callback) => {
    log.debug(`space.new`)
    tokenValidate(token)
    .then(({ profile_id }) => {
      db.spaceNew(profile_id)
      .then(space => {
        callback(null, space)
      })
    })
    .catch(error => {
      callback(error)
    })
  },

  'space.merge': ({ token, space_id, update_ts }, callback) => {
    log.debug(`space.merge ${space_id}`)
    tokenValidate(token)
    .then(({ profile_id }) => {
      db.spaceMerge(space_id, update_ts)
      .then(space => {
        callback(null, space)
      })
    })
    .catch(error => {
      callback(error)
    })
  },

  'note.new': ({ token, space_id, note }, callback) => {
    log.debug(`note.new ${space_id}`)
    tokenValidate(token)
    .then(({ profile_id }) => {
      db.noteNew(space_id, note)
      .then(space => {
        callback(null, space)
      })
    })
    .catch(error => {
      callback(error)
    })
  },

}
