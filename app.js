'use strict'

require('dotenv').config()

const SERVER_PORT = process.env.SERVER_PORT
const FLAG_PROD = process.env.BUILD !== 'development'

const log4js = require('log4js')
log4js.configure({
  appenders: {
    out: { type: 'console' }
  },
  categories: {
    default: { appenders: [ 'out' ], level: 'debug' }
  }
})
const log = log4js.getLogger('app')
const express = require('express')
const app = express()
const http = require('http')
const path = require('path')
const cookieParser = require('cookie-parser')
const createError = require('http-errors')
const jayson = require('jayson')

const routeWeb = require('./routes/index')
const routeApi = require('./routes/api')

app.use(express.json())
app.use(express.urlencoded({ extended: false }))

// routes
app.use(express.static(path.join(__dirname, 'public')))
app.use(cookieParser())
app.post('/api', jayson.server(routeApi).middleware())
app.get('/', routeWeb)

// errors
app.use((req, res, next) => {
	next(createError(404))
})
app.use((err, req, res, next) => {
  // set locals, only providing error in development
  res.locals.title = err.message
  res.locals.error = FLAG_PROD ? err : {}

  // render the error page
  res.status(err.status || 500)
  //res.render('error')
})

// start server
const server = http.createServer(app)
server.listen(SERVER_PORT, () => {
  log.info(`Server started on port ${SERVER_PORT}`)
})

// quit on ctrl-c when running docker in terminal
process.on('SIGINT', () => {
  log.info('Got SIGINT (aka ctrl-c in docker). Graceful shutdown ')
  shutdown()
})

// quit properly on docker stop
process.on('SIGTERM', () => {
  log.info('Got SIGTERM (docker container stop). Graceful shutdown ')
  shutdown()
})

const sockets = {}
let nextSocketId = 0
server.on('connection', (socket) => {
  const socketId = nextSocketId++
  sockets[socketId] = socket
  socket.once('close', () => {
    delete sockets[socketId]
  })
})

// shut down server
function shutdown() {
  waitForSocketsToClose(10)
  server.close((err) => {
    if (err) {
      log.error(err)
      process.exitCode = 1
    }
    log.info('Server stopped')
    process.exit()
  })
}

function waitForSocketsToClose(counter) {
  if (counter > 0) {
    log.info(`Waiting for all connections to close...`)
    return setTimeout(waitForSocketsToClose, 1000, counter - 1)
  }
	
  log.info('Forcing all connections to close now')
  for (var socketId in sockets)
    sockets[socketId].destroy()
}
