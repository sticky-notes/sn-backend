# Структура БД

## auths_generic - авторизации по уникальному ключу

```js
{
  "_id": "INDEX: <external generic guid>",
  "profile_id": "<profile id>"
}
```


## profiles - профили пользователей

```js
{
  "_id": "INDEX: <profile id>",
  "space_ids": [
    "<space id>",
    ...
  }
}
```


## spaces - рабочие пространства

```js
{
  "_id": "INDEX: <space id>",
  "update_ts": 123456789,
  "notes": [
    {
      "_id": "INDEX: <note id>",
      "update_ts": 123456789,
      "media": [
        {
          "type": "text",
          "value": "<note text>",      
        }
      ]
    },
    ...
  ]
}
```


# BACKLOG

```js
  "roles": {
    "own_id": "<profile id>",
    "update_ids": [
      "<profile id>",
      ...
    ],
    "read_ids": [
      "<profile id>",
      ...
    ]
  },
```
