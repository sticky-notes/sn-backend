const DB_USER = process.env.DB_USER
const DB_PASSWORD = process.env.DB_PASSWORD
const DB_URL = process.env.DB_URL

const DB_COLLECTION_AUTH = 'auth'

const log = require('log4js').getLogger('db')

const mongodb = require('mongodb')
const MongoClient = mongodb.MongoClient
const ObjectId = mongodb.ObjectID
const mongoUrl = `mongodb://${DB_USER}:${DB_PASSWORD}@${DB_URL}`
const client = new MongoClient(mongoUrl, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
})

let dbAuths
let dbProfiles
let dbSpaces

client.connect((err, client) => {
  if (err) return log.error(`DB failed at ${mongoUrl}`)

  log.info(`DB connected at ${mongoUrl}`)
  const db = client.db('sticky-notes')
  dbAuths = db.collection('auths_generic')
  dbProfiles = db.collection('profiles')
  dbSpaces = db.collection('spaces')
})

module.exports = {

  authGeneric: guid => {
    return new Promise((resolve, reject) => {
      dbAuths.findOne({ _id: guid })
      .then(authData => {
        if (authData) {
          // account found
          resolve(authData.profile_id)
        } else {
          // create new account
          dbSpaces.insertOne({
            update_ts: Date.now(),
            notes: []
          })
          .then(spaceData => {
            if (spaceData) {
              let space_id = spaceData.insertedId
              dbProfiles.insertOne({
                space_ids: [space_id]
              })
              .then(profileData => {
                if (profileData) {
                  let profile_id = profileData.insertedId
                  dbAuths.insertOne({
                    _id: guid,
                    profile_id
                  })
                  .then(authData => {
                    if (authData) {
                      resolve(profile_id)
                    }
                  })
                }
              })
            }
          })
        }
      })
    })
  },

  spaceList: profileId => {
    return new Promise((resolve, reject) => {
      dbProfiles.findOne({ _id: new ObjectId(profileId) })
      .then(profileData => {
        let space_ids = profileData.space_ids
        dbSpaces.find({ _id: { $in: space_ids } }).toArray()
        .then(spacesData => {
          let result = {}
          if (spacesData) {
            spacesData.forEach(element => result[element._id] = {
              update_ts: element.update_ts
            })
          }
          resolve(result);
        })
      })
    })
  },

  spaceNew: profileId => {
    return new Promise((resolve, reject) => {
      dbSpaces.insertOne({
        update_ts: Date.now(),
        notes: []
      })
      .then(spaceData => {
        let spaceId = spaceData.insertedId
        dbProfiles.updateOne(
          { _id: new ObjectId(profileId) },
          { $push: { space_ids: spaceId } }
        )
        .then(profileData => {
          let space = spaceData.ops[0]
          let result = {}
          result[spaceId] = {
            update_ts: space.update_ts
          }
          resolve(result)
        })
      })
    })
  },

  spaceMerge: (spaceId, ts) => {
    return new Promise((resolve, reject) => {
      dbSpaces.findOne(
        {
          _id: new ObjectId(spaceId),
          update_ts: { $gt: ts }
        }
      )
      .then(spaceData => {
        if (spaceData) {
          // filter notes
          let notes = {}
          spaceData.notes
          .filter(value => value.update_ts > ts)
          .forEach(value => {
            notes[value._id] = {
              update_ts: value.update_ts,
              media: value.media
            }
          })
          let result = {}
          result[spaceData._id] = {
            update_ts: spaceData.update_ts,
            notes
          }
          resolve(result)
        } else {
          // no changes
          resolve({})
        }
      })
    })
  },

  noteNew: (spaceId, note) => {
    return new Promise((resolve, reject) => {
      let ts = Date.now()
      let noteId = new ObjectId()
      note._id = noteId
      note.update_ts = ts
      dbSpaces.updateOne(
        { _id: new ObjectId(spaceId) },
        {
          $set: { update_ts: ts },
          $push: { notes: note }
        }
      )
      .then(noteData => {
        let result = {}
        let lastNote = {}
        result[spaceId] = {
          update_ts: ts,
          notes: lastNote
        }
        lastNote[noteId] = {
          update_ts: ts,
          media: note.media
        }
        resolve(result)
      })
    })
  },

}