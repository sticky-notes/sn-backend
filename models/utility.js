module.exports.tokenCreate = (options, ttl) => {
  return tokenEncode(Object.assign({
    expire_ts: Date.now() + ttl
  }, options))
}

module.exports.tokenValidate = token => {
  return new Promise(function(resolve, reject) {
    let options
    try {
      options = tokenDecode(token)
    } catch (error) {
      reject({
        code: 1,
        message: error
      })
      return
    }
    if (!options) {
      reject({
        code: 2,
        message: 'Empty token'
      })
    } else if (!options.expire_ts || options.expire_ts < Date.now()) {
      reject({
        code: 3,
        message: 'Token expired'
      })
    } else if (!options.profile_id) {
      reject({
        code: 4,
        message: 'No profile reference'
      })
    } else {
      // no error
      resolve(options)
    }
  })
}

function tokenEncode(data) {
  let str = JSON.stringify(data)
  let buf = new Buffer(str)
  return buf.toString('base64')
}

function tokenDecode(data) {
  let buf = new Buffer(data, 'base64')
  let str = buf.toString('ascii')
  return JSON.parse(str)
}
